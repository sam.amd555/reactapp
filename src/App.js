import './App.css';
import Navbar from './components/Navbar';
import Textform from './components/Textform';
import React, { useState } from 'react';

function App() {
const [mode, setmode] = useState('light'); //dark mode is enable or not


const toogleMode = () =>{
  if (mode === 'light') {
    setmode('dark');
    document.body.style.backgroundColor ='#082032';
    document.body.style.color ='#FFA069';
 
  }
  else{
    setmode('light');
    document.body.style.backgroundColor ='#f2f2f2';
    document.body.style.color ='#222';
  
  }
}
  return (
    <>

<Navbar title= "React😍Js" mode={mode} toogleMode={toogleMode}/>
  <div className="container my-3">
  <Textform mode={mode}/>
  
  </div>
  </>
  );
}

export default App;