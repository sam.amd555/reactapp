import React, {useState} from 'react'

export default function Textform(props) {
    const [text, setText] = useState("Enter Your Text Here!");
 const handelUpClick =()=>{
     let newText = text.toUpperCase();
     setText(newText)
 }
 const handelLowClick =()=>{
    let newText = text.toLowerCase();
    setText(newText)
}
const handelClear=()=>{
    let newText ="";
    setText(newText)
}
 const handleOnChange =(e)=>{
    setText(e.target.value)
    
}
    
    return (
        <>
    <div className="container"  style = {{color:props.mode ==='dark'?'light':'dark'}} >
        <h1>{props.heading}</h1>
      <div className ="mb-3">
        <label htmlFor="exampleFormControlTextarea1" className ="form-label">Enter Your Lower-Case Text Below 👇</label>
    <textarea className ="form-control" onChange={handleOnChange} style = {{backgroundColor:props.mode ==='dark'? '#2C394B':'light', color: props.mode=== 'dark'?'#FA8072':'black'}}  value={text} id="myBox" rows="8"></textarea>

      </div>
      <button className= "btn btn-primary mx-2"  onClick={handelUpClick}>To Upper Case</button>
      <button className= "btn btn btn-success mx-2" onClick={handelLowClick}>To Lower Case</button>
      <button className= "btn btn btn-danger mx-2" onClick={handelClear}>New Text</button>

      </div>
            
      <div className="mt-8 split">
      <h5>Total Words: <b>{text.split(" ").length} </b>Total Characters: <b>{text.length}</b></h5>
      <p>{0.007 * text.split(' ').length}Minutes Read</p>
      <h6>Preview</h6>
      <p>{text.length>0?text:"Enter Text Above To Preview"}</p>

      </div>
      </>
    
    )
}
